<?php
/**
 * Created by PhpStorm.
 * User: Luciferus
 * Date: 15.12.2018
 * Time: 16:29
 */
include "autoload/autoload.php";
if (empty($_POST["array_ingredients"]) OR !is_numeric($_POST["latitude"]) OR !is_numeric($_POST["longitude"])) {
    echo json_encode(array("status" => "error", "reason" => "wrongargs"));
    exit();
}

$ingredients = make_ingredient_conditions($_POST["array_ingredients"]);

$r = $_POST["basic_distance"] / 6371;
$lat = deg2rad($_POST["latitude"]);
$lon = deg2rad($_POST["longitude"]);
$delta_lon = asin(sin($r) / cos($lat));
$lat_min = $lat - $r;
$lat_max = $lat + $r;
$lon_min = $lon - $delta_lon;
$lon_max = $lon + $delta_lon;
$ingredients_count = count($_POST["array_ingredients"]);
$data = get_all_pizzas($ingredients, $lat_min, $lat_max, $lon_min, $lon_max, $r, $lat, $lon);

if (!empty($data)) {

    $pizzas_done = array();
    $data_organized = array();
    foreach ($data as $key => $value) {
        $p_id = $value["id"];
        $p_name = $value["name"];
        $p_price = $value["price"];
        $rest_name = $value["restaurant_name"];
        $resturl = $value["menu_url"];
        $latitude = $value["latitude"];
        $longitude = $value["longitude"];
        $distance = round(distance($lat, $lon, $latitude, $longitude));

        if (in_array($p_id, $pizzas_done)) {

            foreach ($data_organized[$rest_name] as $existing_p => $p_val) {

                if ($p_val["pizza_name"] == $p_name) {

                    $data_organized[$rest_name][$existing_p]["occurrences"] = $p_val["occurrences"] + 1;
                    break;

                }
            }
            continue;
        }

        $data_organized[$rest_name][] = array(
                                            "pizza_name" => $p_name,
                                            "pizza_price" => $p_price,
                                            "distance" => $distance,
                                            "occurrences" => 1,
                                            "menu_url" => $resturl
                                            );
        $pizzas_done[] = $p_id;
    }
    $organized_pizzas = organize_pizzas($data_organized, $ingredients_count);
    $organized_pizzas = array_slice($organized_pizzas, 0, 10, true);

    if (!empty($organized_pizzas)) {
        echo json_encode(array("status" => "success", "data" => $organized_pizzas));
    } else {
        echo json_encode(array("status" => "error", "reason" => "nopizzasfound"));
    }
} else {
    echo json_encode(array("status" => "error", "reason" => "nopizzasfound"));
}
