<?php

	session_start();
	require("db.php");

	if (isset($_POST["checked_submit"])) {

		$celek = null;

		if (isset($_POST["checked_rajce"])) {
			$rajce = $_POST["checked_rajce"];
			$rajce = "Rajče";
			$celek[] .= $rajce;
		}

		if (isset($_POST["checked_olivy"])) {
			$olivy = $_POST["checked_olivy"];
			$olivy = "Olivy";
			$celek[] .= $olivy;
		}

		if (isset($_POST["checked_spenat"])) {
			$spenat = $_POST["checked_spenat"];
			$spenat = "Špenát";
			$celek[] .= $spenat;
		}

		if (isset($_POST["checked_cibule"])) {
			$cibule = $_POST["checked_cibule"];
			$cibule = "Cibule";
			$celek[] .= $cibule;
		}

		if (isset($_POST["checked_kukurice"])) {
			$kukurice = $_POST["checked_kukurice"];
			$kukurice = "Kukuřice";
			$celek[] .= $kukurice;
		}

		if (isset($_POST["checked_salam"])) {
			$salam = $_POST["checked_salam"];
			$salam = "Salám";
			$celek[] .= $salam;
		}

		if (isset($_POST["checked_sunka"])) {
			$sunka = $_POST["checked_sunka"];
			$sunka = "Šunka";
			$celek[] .= $sunka;
		}

		if (isset($_POST["checked_klobasa"])) {
			$klobasa = $_POST["checked_klobasa"];
			$klobasa = "Klobása";
			$celek[] .= $klobasa;
		}

		if (isset($_POST["checked_slanina"])) {
			$slanina = $_POST["checked_slanina"];
			$slanina = "Slanina";
			$celek[] .= $slanina;
		}

		if (isset($_POST["checked_krevety"])) {
			$krevety = $_POST["checked_krevety"];
			$krevety = "Krevety";
			$celek[] .= $krevety;
		}

		if (isset($_POST["checked_tunak"])) {
			$tunak = $_POST["checked_tunak"];
			$tunak = "Tuňák";
			$celek[] .= $tunak;
		}

		if (isset($_POST["checked_mozarella"])) {
			$mozarella = $_POST["checked_mozarella"];
			$mozarella = "Mozarella";
			$celek[] .= $mozarella;
		}

		if (isset($_POST["checked_cedal"])) {
			$cedal = $_POST["checked_cedal"];
			$cedal = "Čedar";
			$celek[] .= $cedal;
		}

		if (isset($_POST["checked_gouda"])) {
			$gouda = $_POST["checked_gouda"];
			$gouda = "Gouda";
			$celek[] .= $gouda;
		}

		if (isset($_POST["checked_niva"])) {
			$niva = $_POST["checked_niva"];
			$niva = "Niva";
			$celek[] .= $niva;
		}

		if (isset($_POST["checked_oregano"])) {
			$oregano = $_POST["checked_oregano"];
			$oregano = "Oregáno";
			$celek[] .= $oregano;
		}

		if (isset($_POST["checked_zampiony"])) {
			$zampiony = $_POST["checked_zampiony"];
			$zampiony = "Žampiony";
			$celek[] .= $zampiony;
		}

		if (isset($_POST["checked_feferonky"])) {
			$feferonky = $_POST["checked_feferonky"];
			$feferonky = "Feferonky";
			$celek[] .= $feferonky;
		}

		if (isset($_POST["checked_chilli"])) {
			$chilli = $_POST["checked_chilli"];
			$chilli = "Chilli";
			$celek[] .= $chilli;
		}

		if (isset($_POST["checked_ananas"])) {
			$ananas = $_POST["checked_ananas"];
			$ananas = "Ananas";
			$celek[] .= $ananas;
		}

		$_SESSION["ingredience"] = $celek;

		header("Location: found.php");

	} else {
		echo "nic";
	}

?>