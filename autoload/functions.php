<?php
	function more_information($pizza, $restaurant) {
		global $db;
		$sql = "SELECT pizzas.price, pizza_restaurants.latitude, pizza_restaurants.longitude, ingredients.name FROM `ingbind` 
                JOIN pizzas ON ingbind.pizza_id = pizzas.id 
                JOIN pizza_restaurants ON pizzas.pizzeria_id = pizza_restaurants.id 
                JOIN ingredients ON ingbind.ingredient_id = ingredients.id
                WHERE pizzas.name = '$pizza' AND pizza_restaurants.restaurant_name = '$restaurant'";
		$sqlPriprava = $db->prepare($sql);
		$sqlPriprava->execute();
		$data = $sqlPriprava->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}

	function insert_email_n_code($array_email_n_code) {
	    global $db;
        $sql = "INSERT INTO email_addresses (`email_address`, `unsubscribe_code`) VALUES (:email_address, :unsubscribe_code) ON DUPLICATE KEY UPDATE id=id";
        $pripravu = $db->prepare($sql);
        $pripravu->execute($array_email_n_code);
        $count = $pripravu->rowCount();
        return $count;
    }

    function get_email_addresses() {
        global $db;
        $sql = "SELECT email_address, unsubscribe_code FROM `email_addresses`";
        $sqlPriprava = $db->prepare($sql);
        $sqlPriprava->execute();
        $data = $sqlPriprava->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function make_unsubscribe_code($email_address) {
	    $length = 10;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
	    $sha1_email = sha1($email_address.$randomString);
        return $sha1_email;
    }

    function delete_email($unsubscribe_code) {
        global $db;
        $sql = "DELETE FROM email_addresses WHERE unsubscribe_code = :ucode";
        $pripravu = $db->prepare($sql);
        $pripravu->execute([":ucode" => $unsubscribe_code]);
    }

    function notification_message($unsubscribe_code) {
        $finished_message = "";
        $needles = array("{#nadpis1#}", "{#zprava#}", "{#unsubscribe_message#}");
        $replace_for = array(
            "NAJDIPIZZU",
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
             Nulla non arcu lacinia neque faucibus fringilla. Fusce nibh. 
             Pellentesque sapien. Mauris metus. 
             Phasellus et lorem id felis nonummy placerat. 
             Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Sed vel lectus. 
             Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Aenean placerat. 
             Pellentesque pretium lectus id turpis. 
             Nulla turpis magna, cursus sit amet, suscipit a, interdum id, felis.",
            "Pokud chcete odhlásit odběr novinek, klikněte prosím na tento odkaz: <a href=\"https://nomoneyforarealdomain.tk/notifications/sign_off.php?key=".$unsubscribe_code . "\" title=\"Odhlášení zpráv\">Odhlásit odběr novinek</a>"
        );
        $content = file_get_contents("email_templates/notification_message.html");
        $finished_message = str_replace($needles, $replace_for, $content);
        return $finished_message;
    }

    function search_bar($searched) {
	    global $db;
        $sql = "SELECT DISTINCT name FROM pizzas WHERE name LIKE :tosearch";
        $sqlPriprava = $db-> prepare($sql);
        $sqlPriprava->execute([":tosearch" => $searched . "%"]);
        $data = $sqlPriprava->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function get_pizzas_by_name($pizza_name, $lat_min, $lat_max, $lon_min, $lon_max, $r, $lat, $lon, $get = false) {
        global $db;
        //$sql = "SELECT * FROM pizzas";
        $sql = $get ? "SELECT * FROM pizzas" : "SELECT DISTINCT name FROM pizzas";
        $sql .= " JOIN pizza_restaurants ON pizzas.pizzeria_id = pizza_restaurants.id";
        $sql .= $get ? " WHERE pizzas.name = :pizzaname" : " WHERE pizzas.name LIKE :pizzaname";
        $sql .=" AND (RADIANS(pizza_restaurants.latitude) >= $lat_min 
                 AND RADIANS(pizza_restaurants.latitude) <= $lat_max) 
                 AND (RADIANS(pizza_restaurants.longitude) >= $lon_min 
                 AND RADIANS(pizza_restaurants.longitude) <= $lon_max) 
                 AND acos(sin($lat) * sin(RADIANS(pizza_restaurants.latitude)) + cos($lat) * cos(RADIANS(pizza_restaurants.latitude)) * cos(RADIANS(pizza_restaurants.longitude) - ($lon))) <= $r";

        $sqlPriprava = $db->prepare($sql);
        $sqlPriprava->execute([":pizzaname" => $get ? $pizza_name : $pizza_name . "%"]);
        $data = $sqlPriprava->fetchAll(PDO::FETCH_ASSOC);

        if (!$get) {
            return $data;
        }

        $toreturn = [];
        foreach ($data as $key => $value) {
            $p_name = $value["name"];
            $p_price = $value["price"];
            $rest_name = $value["restaurant_name"];
            $resturl = $value["menu_url"];
            $latitude = $value["latitude"];
            $longitude = $value["longitude"];
            $distance = round(distance($lat, $lon, $latitude, $longitude));
            $toreturn[] = array(
                "pizza_name" => $p_name,
                "pizza_price" => $p_price,
                "pizzeria_name" => $rest_name,
                "distance" => $distance,
                "menu_url" => $resturl
            );
        }
        return $toreturn;
    }

    function distance($latitude_from, $longitude_from, $latitude_to, $longitude_to) { //spočítání vzdálenosti dvou bodů
        // převede stupně na radiany
        $earth_radius = 6371000;
        $lat_from = $latitude_from;
        $lon_from = $longitude_from;
        $lat_to = deg2rad($latitude_to);
        $lonTo = deg2rad($longitude_to);

        $lat_delta = $lat_to - $lat_from;
        $lon_delta = $lonTo - $lon_from;

        $angle = 2 * asin(sqrt(pow(sin($lat_delta / 2), 2) +
                cos($lat_from) * cos($lat_to) * pow(sin($lon_delta / 2), 2)));
        return $angle * $earth_radius;
    }

    function get_all_pizzas($ingredients, $lat_min, $lat_max, $lon_min, $lon_max, $r, $lat, $lon) {
        global $db;
        $sql = "SELECT pizzas.id, pizzas.name, pizza_restaurants.restaurant_name, pizza_restaurants.menu_url, pizza_restaurants.latitude, pizza_restaurants.longitude, pizzas.price FROM `ingbind` 
                JOIN pizzas ON ingbind.pizza_id = pizzas.id 
                JOIN pizza_restaurants ON pizzas.pizzeria_id = pizza_restaurants.id
                WHERE ".$ingredients.
                " AND (RADIANS(pizza_restaurants.latitude) >= $lat_min 
                AND RADIANS(pizza_restaurants.latitude) <= $lat_max) 
                AND (RADIANS(pizza_restaurants.longitude) >= $lon_min 
                AND RADIANS(pizza_restaurants.longitude) <= $lon_max) 
                AND acos(sin($lat) * sin(RADIANS(pizza_restaurants.latitude)) + cos($lat) * cos(RADIANS(pizza_restaurants.latitude)) * cos(RADIANS(pizza_restaurants.longitude) - ($lon))) <= $r ORDER BY `pizza_id` ASC";
        $sqlPriprava = $db->prepare($sql);
        $sqlPriprava->execute();
        $data = $sqlPriprava->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function make_ingredient_conditions($array) {
        $count = count($array);
        $x = 1;
        $condition = "(";
        foreach ($array as $key => $value) {
            $condition .= "ingbind"."."."ingredient_id = ".$value;
            if ($count != $x) {
                $condition .= " OR ";
            }
            $x++;
        }
        $condition .= ")";
        return $condition;
    }

    function organize_pizzas($pizza_array, $ingredients_count) {
	    $result_arr = array();

	    foreach($pizza_array as $pizzeria => $pizzas) {
	        foreach ($pizzas as $pizza => $pizza_val) {
	            if ($pizza_val["occurrences"] < $ingredients_count) {
	                continue;
                }
	            $pizza_val["pizzeria_name"] = $pizzeria;
	            //$pizza_val["menu_url"] =
	            $result_arr[] = $pizza_val;
	        }
        }
        $keys = array_map(function($val) {return $val["occurrences"];}, $result_arr);
	    array_multisort($keys,SORT_DESC, SORT_NUMERIC, $result_arr);

        return $result_arr;
    }



   
