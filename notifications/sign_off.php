<?php
/**
 * Created by PhpStorm.
 * User: Luciferus
 * Date: 14.12.2018
 * Time: 19:30
 */
include "../autoload/autoload.php";

if (!empty($_GET["key"])) {
    $code = htmlspecialchars(strip_tags($_GET["key"]));
    delete_email($code);
}
header("Location: https://nomoneyforarealdomain.tk/");