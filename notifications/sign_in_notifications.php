<?php
/**
 * Created by PhpStorm.
 * User: Luciferus
 * Date: 29.11.2018
 * Time: 19:25
 */
    require 'PHPMailerAutoload.php';
    include "../autoload/autoload.php";
    if (filter_var($_POST["email_address"], FILTER_VALIDATE_EMAIL)) {
        $unsubscribe_code = make_unsubscribe_code($_POST["email_address"]);
        $array_email_n_code = array(':email_address' => $_POST["email_address"], ':unsubscribe_code' => $unsubscribe_code);
        $count = insert_email_n_code($array_email_n_code);
        
        if ($count != 1) {
           echo "Tato e-mailová adresa už je používána";
        } else {
            echo "Úspěšné přihlášení";
        }
    } else {
        echo "Špatný formát e-mailu";
    }
?>