<?php
    include "autoload/autoload.php";
    if (empty($_GET["pizza"]) || !is_numeric($_GET["latitude"]) || !is_numeric($_GET["longitude"])) {
        echo json_encode([]);
        exit();
    }
	$searched = $_GET["pizza"];
    $latitude = $_GET["latitude"];
    $longitude = $_GET["longitude"];
    $r = 20/6371; //Pevně nastavených 20 km
    $lat = deg2rad($latitude);
    $lon = deg2rad($longitude);
    $delta_lon = asin(sin($r) / cos($lat));
    $lat_min = $lat - $r;
    $lat_max = $lat + $r;
    $lon_min = $lon - $delta_lon;
    $lon_max = $lon + $delta_lon;

    if (!empty($_GET["action"]) && $_GET["action"] == "get") {

        $data = get_pizzas_by_name($searched, $lat_min, $lat_max, $lon_min, $lon_max, $r, $lat, $lon, true);

        echo json_encode($data);
    }
	else {
        $data = get_pizzas_by_name($searched, $lat_min, $lat_max, $lon_min, $lon_max, $r, $lat, $lon, false);

        echo json_encode($data);
    }



