$( document ).ready(function() {

	// SEARCH

	$("#about_form").on("submit", function(e) {
		e.preventDefault();
	});

	var timer;
	$( "#about_form_input" ).on("keyup", function() {
		//Aby to chvilku počkalo než začne hledat
		clearTimeout(timer);
        var text = $("#about_form_input").val();

        if (text.length < 1 || text.replace(/\s+/g, "").length < 1) { //Dodělanej fix aby tam nešla prcnout jenom mezera a vyhodilo všechny pizzy
            $("#searched_box").hide();
            return;
        }
		timer = setTimeout(function() {

            $.get("findbyname.php?pizza=" + text + "&action=search&latitude=" + cl_lat + "&longitude=" + cl_long, function(data, status){
                var array = JSON.parse(data);

                if (array.length < 1) {
                    //console.log("Pizza nebyla nalezena");
                    $("#searched_box").empty();
                    //$("#searched_box").append("<div class='searched_pizza'>" + "<p class='seached_empty'>" + "Pizza nebyla nalezena" + "</p>" + "</div>");
					return;
                }

                $("#searched_box").empty();

                for (var i in array) {
                    var pizza = array[i]["name"];
                    pizza = pizza.charAt(0).toUpperCase() + pizza.substr(1);

                    $("#searched_box").append("<a href='#'  class='searched_a' data-pname=\"" + pizza.toLowerCase() + "\">" + "<div class='searched_pizza'>" + "<p class='seached_text'>" + pizza + "</p>" + "</div>" + "</a>");
                }
                $("#searched_box").show();

                $(".searched_a").unbind("click");

                $(".searched_a").on("click", function(e) {
                	e.preventDefault();
                	if (this.hasAttribute("data-pname")) {
                		printByName($(this).attr("data-pname"));
					}
				});
            });
		}, 250);


	});

	// CHECK

	$(".selector_button").unbind("click").click(function() {
		var ingredience = $(this).attr("data-ing_for");
		
		if (!checkarr.includes(ingredience)) {
			checkarr.push(ingredience);
			$(this).addClass("colored");
		} else {
			remove(checkarr, ingredience);
			$(this).removeClass("colored");
	}
        var findbtn = $("#find_button");
		if (checkarr.length < 1) {

            findbtn.html("NELZE HLEDAT");
            findbtn.prop("disabled", true);
			$("#selector_header").html("Vyberte si přísady");
            findbtn.addClass("colored_button");
		} else {
            findbtn.html("HLEDAT PIZZU");
			$("#selector_header").empty();
            findbtn.removeClass("colored_button");

            findbtn.prop("disabled", false);

			var ingredience_str = "";

			for (var i = 0; i < checkarr.length; i++) {
				nazev = $("div[data-ing_for=" + checkarr[i] + "]").text();
                if (ingredience_str.length + nazev.length >= 40){ //Tečky když je tam více ingrediencí aby to nevypadalo blbě
                    ingredience_str += " ...";
                    break;
                }
				if (i != checkarr.length - 1) {
					ingredience_str += (nazev + ", ");
				} else {
					ingredience_str += nazev;
				}
			}

			$("#selector_header").html("Vybrané přísady:" + "</h3>" + "<p class='find_p'>" + ingredience_str + "</p>");
		}

	});

});

function printByName(pizzaname) {
	$.get("findbyname.php?pizza=" + pizzaname + "&action=get&latitude=" + cl_lat + "&longitude=" + cl_long, function(data, status) {
		var arr = JSON.parse(data);
		printResultPizzas(arr);
	});
}

var checkarr = new Array();

function remove(array, element) {
	const index = array.indexOf(element);

	if (index !== -1) {
		array.splice(index, 1);
	}
}

$(document).on("click", function (e) {
    if ($(e.target).parents("#searched_box").length === 0) {
        $("#searched_box").hide();
    }
});

