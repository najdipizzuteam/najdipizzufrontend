var array_ingredients = [];
var cl_lat = 0;
var cl_long = 0;
var basic_distance = 5;

function load_doc(position) {
    cl_lat = position.coords.latitude;
    cl_long = position.coords.longitude;
}

function post_data(lat, long, array_ingredients) {
    $.post("find_pizza_process.php", {latitude:lat, longitude:long, array_ingredients : array_ingredients, basic_distance: basic_distance}).done(function(data) {
        var pizza_array = JSON.parse(data);
        if (pizza_array.status === "error") {
            if (pizza_array.reason === "nopizzasfound") {
                if (basic_distance >= 20) {
                    alert("Dosažen limit vzdálenosti vyhledávání" + "\nZkuste zvolit jinou kombinaci ingrediencí");
                    basic_distance = 5;
                    return;
                }

                basic_distance = basic_distance + 5;
                post_data(lat, long, array_ingredients);

                return;
            } else {
                alert("Něco se pokazilo");
                return;
            }
        }
        basic_distance = 5;
        printResultPizzas(pizza_array.data);



    });
}

function printResultPizzas(pizzasArray) {
    var pizzasfound_html = "";
    for (var pizza in pizzasArray) {

        var pname = pizzasArray[pizza].pizza_name;
        var p_restaurant = pizzasArray[pizza].pizzeria_name;
        var p_url = pizzasArray[pizza].menu_url;
        var p_prize = pizzasArray[pizza].pizza_price;
        var p_distance = pizzasArray[pizza].distance;
        pizzasfound_html += "<div class=\"detailed_pizza\" data-url=\"" + p_url + "\">";
        pizzasfound_html += "<h3 class='detailed_name'>" + pname + "<h3>";
        pizzasfound_html += "<div class=\"more_details\">";
        pizzasfound_html += "<p class='detailed_info pizzeria'>" + p_restaurant + "</p>";

        if (p_distance > 999) {
            var p_distance_in_km = p_distance / 1000;
            pizzasfound_html += "<p class='detailed_info distance'>" + "Vzdálenost: "+ Math.round(p_distance_in_km) + " km" + "</p>";
        } else {
            pizzasfound_html += "<p class='detailed_info distance'>" + "Vzdálenost: "+ Math.round(p_distance) + " m" + "</p>";
        }
        pizzasfound_html += "<p class='detailed_info price'>" + "Cena: " + "<span class='pricenumber'>" + p_prize + " Kč" + "</span>" + "</p>";
        pizzasfound_html += "</div>";
        pizzasfound_html += "</div>";

    }
    $("#pizzas").html(pizzasfound_html);
    $("#found_box").show();

    //Unbinduje klikací listenery aby to nedělalo bordel
    $(".detailed_pizza").unbind("click");

    //zase je nabinduje u nových elementů
    $(".detailed_pizza").on("click", function() {
        if (this.hasAttribute("data-url")) {
            window.open($(this).attr("data-url"));
        }
    });

    scrollizi();
}

function geolocError() {
    alert("Poloha nebyla povolena.\nNastavení výchozí polohy: Někde v centru Prahy");
    cl_lat = 50.087043;
    cl_long = 14.421721;
}

function get_location() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(load_doc, geolocError);
    } else {
        console.log("Geolokace není podporována webovým prohlížečem");
        geolocError();
    }
}

//Scrolluje dolů k výsledkům pizzy
function scrollizi() {
    $([document.documentElement, document.body]).animate({
        scrollTop: $("#found_box").offset().top
    }, 200);
}

$(document).ready(function() {

    get_location();

    $(".selector_button").on("click", function(event) {
        var id = $(this).attr('data-ing_id');

        if ($.inArray(id, array_ingredients) != -1) {
            array_ingredients.splice( $.inArray(id, array_ingredients), 1 );
        } else {
            array_ingredients.push(id);
        }
    });

    $("#find_button").on("click", function (event) {
        event.preventDefault();
        if (array_ingredients.length < 1) {  // Podmínka zkontroluje, zda jsou vybrany nějaké ingredience
            return;
        } else {
            post_data(cl_lat, cl_long, array_ingredients);
        }
    });


});

