$(document).ready(function() {
    $("#header_email_submit").on("click", function(event) {
        event.preventDefault();
        var email_address = $("#header_email_form").val();
        if (!isEmail(email_address)) {
            $("#header_email")[0].reset();
            $("#header_email_form").prop("placeholder", "Chybný formát");
            return;
        }
        $.post("notifications/sign_in_notifications.php", {email_address : email_address}).done(function(data) {
            $("#header_email")[0].reset();
            $("#header_email_form").prop("placeholder", data);
		});
    });
});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
